#!/bin/sh

case ${1:--h} in
	-h|-help|--help)
		echo "USAGE: ${0##*/} HOST[(:DISPLAY#|::PORT#)]"
		exit 0
		;;
	-*)
		echo "Invalid option '$1'. Try '${0##*/} --help'."
		exit 1
		;;
	*::*)
		HOST=${1%::*}
		PORT=${1#*::}
		;;
	*:*)
		HOST=${1%:*}
		DSPL=${1#*:}
		PORT=$((5900+DSPL))
		;;
	*)
		HOST="$1"
		PORT=5900
		;;
esac

echo $HOST $PORT

export CLASSPATH=/usr/share/java/tightvncviewer.jar

java tightvncviewer.VncViewer HOST $HOST PORT $PORT
